# minimum required CMAKE version
CMAKE_MINIMUM_REQUIRED(VERSION 3.7 FATAL_ERROR)

project(CBB)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

# compiler must be 11 or 14
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_COMPILER g++)

# required if linking to static library
add_definitions(-DANTLR4CPP_STATIC)

# using /MD flag for antlr4_runtime (for Visual C++ compilers only)
set(ANTLR4_WITH_STATIC_CRT OFF)
# add external build for antlrcpp
include(ExternalAntlr4Cpp)
# add antrl4cpp artifacts to project environment
include_directories(${ANTLR4_INCLUDE_DIRS})

# set variable pointing to the antlr tool that supports C++
# this is not required if the jar file can be found under PATH environment
set(ANTLR_EXECUTABLE /home/thomas/antlr4/antlr-4.9-complete.jar)
# add macros to generate ANTLR Cpp code from grammar
find_package(ANTLR REQUIRED)

antlr_target(CBBParser Cbb.g4 PARSER LEXER LISTENER VISITOR
             PACKAGE cbb)

# include generated files in project environment
include_directories(${ANTLR_CBBParser_OUTPUT_DIR})

file(GLOB cbb_SRC "src/*.cpp")

# add generated grammar to demo binary target
add_executable(cbb src/cbb.cpp src/parser.cpp src/semantic_validator.cpp
               ${ANTLR_CBBParser_CXX_OUTPUTS})
target_link_libraries(cbb antlr4_static)