grammar Cbb;

file
    : TEST? MODULE IDENTIFIER (';'|NEWLINE) NEWLINE* declList? NEWLINE* EOF;

qualifiedName
    : (anyIdentifier NEWLINE* '.')* NEWLINE* IDENTIFIER;

anyIdentifier
    : IDENTIFIER | TYPE_IDENTIFIER;

visibilityModifier
    : 'export'
    | 'hide' 
    ;

declList
    : decl (NEWLINE* decl)*;

decl
    : visibilityModifier? varDecl
    | visibilityModifier? funcDecl
    | visibilityModifier? structDecl
    ;

structDecl
    : 'struct' TYPE_IDENTIFIER NEWLINE? '{' NEWLINE* declList NEWLINE* '}';

varDecl
    : ('var'|'val') IDENTIFIER (':' type)? ('=' expr)?;

funcDecl
    : funcModifier* IDENTIFIER '(' paramList? ')' type NEWLINE* funcBlock? ;

funcModifier
    : 'pure';

funcBlock
    : '{' NEWLINE* stmtList? NEWLINE* '}';

paramList
    : param NEWLINE* (',' NEWLINE* param)*;

param
    : (IDENTIFIER ':')? type;

stmtList
    : stmt (NEWLINE* stmt)*;

stmt
    : assStmt stmtTerminator
    | varDecl stmtTerminator
    | returnStmt stmtTerminator
    | blockStmt
    | ifStmt
    | unlessStmt
    | loopStmt
    | labelStmt COLON
    ;

loopStmt
    : whileStmt
    | untilStmt
    | forStmt
    | doWhileStmt stmtTerminator
    | doUntilStmt stmtTerminator
    ;

doWhileStmt
    : 'do' NEWLINE* blockStmt 'while' expr;

doUntilStmt
    : 'do' NEWLINE* blockStmt 'until' expr;

forStmt
    :'for' (varDecl|assStmt)*';'expr?';'stmt NEWLINE* blockStmt;

untilStmt
    : 'until' expr NEWLINE* blockStmt;

whileStmt
    : 'while' expr NEWLINE* blockStmt;

blockStmt
    : '{' NEWLINE* stmtList? NEWLINE* '}';

ifStmt
    : 'if' expr blockStmt ('elif' expr blockStmt)* ('else' blockStmt)?;

unlessStmt
    : 'unless' expr blockStmt ('thun' expr blockStmt)* ('then' blockStmt)?;

labelStmt
    : '!' IDENTIFIER;

stmtTerminator
    : (NEWLINE|';');

assStmt
    : qualifiedName '=' expr;

assList
    : assStmt (NEWLINE* COMMA NEWLINE* assStmt)*;

returnStmt
    : ('return'|'error') expr;

type
    : typeModifier* typeName typeArgs?;

typeArgs
    : '<' (type COMMA)* type '>';

typeModifier
    : AMPERSAND
    | arrayTypeModifier;

arrayTypeModifier
    : '[' (INT_LITERAL|AMPERSAND)? ']';

qualifiedType
    : (IDENTIFIER NEWLINE DOT)* (TYPE_IDENTIFIER NEWLINE DOT)* shortName=TYPE_IDENTIFIER;

typeName
    : 'I8'
    | 'I16'
    | 'I32'
    | 'I64'
    | 'U8'
    | 'U16'
    | 'U32'
    | 'U64'
    | 'F32'
    | 'F64'
    | 'F80'
    | 'Void'
    | 'Bool'
    | 'String'
    | qualifiedType;

expr 
    // primary expressions
    : qualifiedName
    | arrayExpr
    | structExpr
    | TRUE
    | FALSE
    | SELF
    | INT_LITERAL
    | STRING_LITERAL
    | BIN_INT_LITERAL
    | HEX_INT_LINTERAL
    | FLOAT_LITERAL
    | CHAR_LITERAL
    | parenthesizedExpr
    // postfix expressions
    | expr0=expr postfixOp
    // postfix inc dec
    | expr0=expr ('++' | '--')
    // prefix expressions
    | dereference='*' expr0=expr
    | addressof='&' expr0=expr
    | prefixInc='++' expr0=expr
    | prefixDec='--' expr0=expr
    // factor expression
    | prefixMinus='-' expr0=expr
    | prefixPlus='+' expr0=expr
    // arithmetic
    | expr0=expr mulop=('*' | '/' | '%') expr1=expr
    | expr0=expr addop=('+' | '-') expr1=expr
    // bitshifts
    | expr0=expr bitShift=('<<' | '>>') expr1=expr
    | expr0=expr bitAnd='&' expr1=expr
    | expr0=expr bitOr='|' expr1=expr
    | expr0=expr bitXor='^' expr1=expr
    // comparissons
    | expr0=expr compop=('==' | '>=' | '<=' | '<' | '>') expr1=expr
    // boolean
    | notop='not' expr0=expr
    | expr0=expr booland='and' expr1=expr
    | expr0=expr boolor='or' expr1=expr
    | expr0=expr boolxor='xor' expr1=expr
    | expr0=expr ('if' expr1=expr 'else' | 'unless' expr1=expr 'then' ) expr2=expr
    ;

parenthesizedExpr
    : '(' expr ')';

postfixOp
    : BRACKET_OPEN subscript BRACKET_CLOSED
    | '(' exprList? ')'
    | DOT expr
    ;

structExpr 
    : (qualifiedType | STRUCT)? CURLY_OPEN assList CURLY_CLOSED
    ;


exprList
    : expr (',' expr)*;

subscript
    : INT_LITERAL (COLON INT_LITERAL?)?
    | COLON INT_LITERAL?;

arrayExpr
    : BRACKET_OPEN exprList? BRACKET_CLOSED;

// Lexer
TEST: 'test' ;
MODULE: 'module' ;
STRUCT: 'struct' ;
TRUE: 'true';
FALSE: 'false';
SELF: 'self';

AMPERSAND : '&' ;
BRACKET_OPEN : '[' ;
BRACKET_CLOSED : ']' ;
CURLY_OPEN : '{' ;
CURLY_CLOSED : '}' ;
DOT : '.' ;
DOT_DOT: '..' ;
COLON : ':' ;
COMMA: ',' ;


// expressions
IDENTIFIER : [a-z_][a-zA-Z_0-9]* ;
TYPE_IDENTIFIER: [A-Z][a-zA-Z_0-9]* ;
INT_LITERAL: [0-9]+ ;
BIN_INT_LITERAL: '0b'('0'|'1')+;
HEX_INT_LINTERAL : '0x'[0-9a-fA-F]+;
FLOAT_LITERAL: [0-9]*'.'[0-9]+(('e'|'E')'-'?[0-9]+)? ;
STRING_LITERAL: '"' ~'"'* '"';
CHAR_LITERAL: '\'' (~[\t\n\r'] | '\\'~[\t\n\r']) '\'';

LINECOMMENT: '//' ~'\n'* -> skip;
NEWLINE: '\r'? '\n' ;
WS: [ \r\t]+ -> skip ;