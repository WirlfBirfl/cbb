#include "semantic_validator.h"

#include "antlr4-runtime.h"

#include "CbbParser.h"

#include "string"
#include "vector"

using namespace cbb;

antlrcpp::Any SemanticValidator::visitFile(CbbParser::FileContext *ctx) {
    Module *m = new Module();
    m->name = ctx->IDENTIFIER()->getText();
    m->scope.parent = nullptr;
    m->scope.symbols = std::vector<Symbol>();
    m->exports = std::vector<Symbol>();
    this->scopes.push(&m->scope);
    for(auto& declCtx : ctx->declList()->decl()) {
        auto decl = visitDecl(declCtx).as<Declaration*>();
        m->decls.push_back(decl);
    }
    return m;
}

antlrcpp::Any SemanticValidator::visitDecl(CbbParser::DeclContext *ctx) {
    DeclarationVisibility vis = this->defaultVisibility;
    if(ctx->visibilityModifier()) {
        if(ctx->visibilityModifier()->getText() == "export") {
            vis = DeclarationVisibility::EXPORT;
        } else if(ctx->visibilityModifier()->getText() == "hide") {
            vis = DeclarationVisibility::HIDDEN;
        }
    }
    Declaration *decl = new Declaration();
    decl->visibility = vis;
    if(ctx->funcDecl()) {
        auto func = visitFuncDecl(ctx->funcDecl()).as<FuncDecl*>();
        decl->kind = DeclKind::FUNCTION;
        decl->decl.func = func;
    } else if(ctx->varDecl()) {
        decl->kind = DeclKind::VARIABLE;

    } else if(ctx->structDecl()) {
        decl->kind = DeclKind::STRUCT;

    }
    return decl;
}

antlrcpp::Any SemanticValidator::visitParamList(CbbParser::ParamListContext *ctx) {
    std::vector<Parameter*> params;
    for(auto& paramContext : ctx->param()) {
        params.push_back(visitParam(paramContext).as<Parameter*>());
    }
    return params;
}

antlrcpp::Any SemanticValidator::visitParam(CbbParser::ParamContext *ctx) {
    Parameter *param = new Parameter();
    param->name = ctx->IDENTIFIER()->getText();
    param->type = visitType(ctx->type()).as<Type*>();
    return param;
}

antlrcpp::Any SemanticValidator::visitFuncDecl(CbbParser::FuncDeclContext *ctx) {
    FuncDecl *func = new FuncDecl();
    func->name = ctx->IDENTIFIER()->getText();
    func->symbols.parent = this->scopes.top();
    func->params = visitParamList(ctx->paramList()).as<std::vector<Parameter*>>();
    if(ctx->funcModifier().size() > 0) {
        func->modifier = FunctionModifier::PURE;
    }
    return func;
}

antlrcpp::Any SemanticValidator::visitType(CbbParser::TypeContext *ctx) {
    Type *type = new Type();
    type->kind = TypeKind::NORMAL;
    BaseType *bt = new BaseType();
    bt->name = visitTypeName(ctx->typeName()).as<TypeName>();
    bt->typeArgs = visitTypeArgs(ctx->typeArgs()).as<std::vector<Type*>>();
    type->type.baseType = bt;
    for(int i = ctx->typeModifier().size() - 1; i >= 0; i--) {
        auto t = visitTypeModifier(ctx->typeModifier()[i]).as<ModifiedType*>();
        t->child = type;
        type = new Type();
        type->kind = TypeKind::MODIFIED;
        type->type.modified = t;
    }
    return type;
}

antlrcpp::Any SemanticValidator::visitQualifiedType(CbbParser::QualifiedTypeContext *ctx) {
    TypeName tn;
    tn.qualifiers = std::vector<std::string>();
    tn.basicName = ctx->TYPE_IDENTIFIER()[ctx->TYPE_IDENTIFIER().size() - 1]->getText();
    auto typeIdRevIt = ctx->TYPE_IDENTIFIER().rbegin();
    typeIdRevIt++;
    while(typeIdRevIt != ctx->TYPE_IDENTIFIER().rend()) {
        tn.qualifiers.push_back((*typeIdRevIt)->getText());
        typeIdRevIt++;
    }
    auto idRevIt = ctx->IDENTIFIER().rbegin();
    while(idRevIt != ctx->IDENTIFIER().rend()) {
        tn.qualifiers.push_back((*idRevIt)->getText());
        idRevIt++;
    }
    return tn;
}

antlrcpp::Any SemanticValidator::visitTypeName(CbbParser::TypeNameContext *ctx) {
    if(ctx->qualifiedType()) {
        return visitQualifiedType(ctx->qualifiedType());
    } else {
        TypeName tn;
        tn.qualifiers = std::vector<std::string>();
        tn.basicName = ctx->children[0]->getText();
        return tn;
    }
}

antlrcpp::Any SemanticValidator::visitTypeArgs(CbbParser::TypeArgsContext *ctx) {
    std::vector<Type*> types;
    if(ctx != nullptr)
    for(auto& typeCtx : ctx->type()) {
        types.push_back(visitType(typeCtx).as<Type*>());
    }
    return types;
}

antlrcpp::Any SemanticValidator::visitExprList(CbbParser::ExprListContext *ctx) {
    std::vector<Expr*> exprs;
    for(auto &expr : ctx->expr()) {
        exprs.push_back(visitExpr(expr).as<Expr*>());
    }
    return exprs;
}


antlrcpp::Any SemanticValidator::visitExpr(CbbParser::ExprContext *ctx) {
    Expr *e = new Expr();
    if(ctx->qualifiedName()) {
        e->kind = ExprKind::NAMED;
        e->expr.namedExpression = visitQualifiedName(ctx->qualifiedName()).as<QualifiedName*>();
    } else if(ctx->arrayExpr()) {
        e->kind = ExprKind::ARRAY;
        e->expr.arrayExpr = visitArrayExpr(ctx->arrayExpr()).as<ArrayExpr*>();
    } else if(ctx->structExpr()) {
        e->kind = ExprKind::STRUCT;
        e->expr.structExpr = visitStructExpr(ctx->structExpr()).as<StructExpr*>();
    } else if(ctx->TRUE()) {
        e->kind = ExprKind::LITERAL;
        LiteralExpr *le = new LiteralExpr();
        le->kind = LiteralExprKind::BOOL;
        le->expr.boolLiteral = 1;
        e->expr.literalExpr = le;
    } else if(ctx->FALSE()) {
        e->kind = ExprKind::LITERAL;
        LiteralExpr *le = new LiteralExpr();
        le->kind = LiteralExprKind::BOOL;
        le->expr.boolLiteral = 0;
        e->expr.literalExpr = le;
    } else if(ctx->parenthesizedExpr()) {
        return visitExpr(ctx->parenthesizedExpr()->expr());
    }
    return e;
}

antlrcpp::Any SemanticValidator::visitStructExpr(CbbParser::StructExprContext *ctx) {
    StructExpr *strExpr = new StructExpr();
    strExpr->name = nullptr;
    strExpr->assignments = visitAssList(ctx->assList()).as<std::vector<AssignmentStmt*>>();
    if(ctx->qualifiedType()) {
        strExpr->name = visitQualifiedType(ctx->qualifiedType()).as<TypeName*>();
    }
    return strExpr;
}

antlrcpp::Any SemanticValidator::visitArrayExpr(CbbParser::ArrayExprContext *ctx) {
    ArrayExpr *arrayExpr = new ArrayExpr();
    if(ctx->exprList()) {
        arrayExpr->expressions = visitExprList(ctx->exprList()).as<std::vector<Expr*>>();
    } else {
        arrayExpr->expressions = std::vector<Expr*>();
    }
    return arrayExpr;
}

antlrcpp::Any SemanticValidator::visitQualifiedName(CbbParser::QualifiedNameContext *ctx) {
    QualifiedName *qn = new QualifiedName();
    qn->qualifiers = std::vector<std::string>();
    qn->basicName = ctx->IDENTIFIER()->getText();
    for(auto& id : ctx->anyIdentifier()) {
        std::string name;
        if(id->TYPE_IDENTIFIER()) {
            name = id->TYPE_IDENTIFIER()->getText();
        } else {
            name = id->IDENTIFIER()->getText();
        }
        qn->qualifiers.push_back(name);
    }
    return qn;
}

antlrcpp::Any SemanticValidator::visitTypeModifier(CbbParser::TypeModifierContext *ctx) {
    ModifiedType *mt = new ModifiedType();
    mt->child = nullptr;
    if(ctx->AMPERSAND() != nullptr) {
        mt->kind = ModifiedTypeKind::POINTER;
        mt->length = 1;
    } else {
        if(ctx->arrayTypeModifier()->INT_LITERAL()) {
            mt->kind = ModifiedTypeKind::ARRAY;
            mt->length = std::stoi(ctx->arrayTypeModifier()->INT_LITERAL()->getText());
        } else if(ctx->arrayTypeModifier()->AMPERSAND()) {
            mt->kind = ModifiedTypeKind::MULTIPOINTER;
            mt->length = 0;
        } else {
            mt->kind = ModifiedTypeKind::SLICE;
            mt->length = 0;
        }
    }
    return mt;
}

antlrcpp::Any SemanticValidator::visitAssList(CbbParser::AssListContext *ctx) {
    std::vector<AssignmentStmt*> stmts;
    for(auto& assStmt : ctx->assStmt()) {
        stmts.push_back(visitAssStmt(assStmt).as<AssignmentStmt*>());
    }
    return stmts;
}

antlrcpp::Any SemanticValidator::visitAssStmt(CbbParser::AssStmtContext *ctx) {
    AssignmentStmt *assStmt = new AssignmentStmt();
    assStmt->assignee = visitQualifiedName(ctx->qualifiedName()).as<QualifiedName*>();
    assStmt->assignedValue = visitExpr(ctx->expr()).as<Expr*>();
    return assStmt;
}
