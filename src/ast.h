#ifndef CBB_AST_H
#define CBB_AST_H

#include <string>
#include <vector>

namespace cbb {

struct Declaration;
struct StructDeclaration;
struct DataDeclaration;
struct VariableDeclaration;
struct FunctionDeclaration;
struct TypeDef;

struct File {
    std::string moduleName;
    std::vector<Declaration*> declarations;

    File(std::string moduleName) : moduleName{moduleName}, declarations{}
    {

    }; 

};

enum class DeclarationKind {
    STRUCT, DATA, VARIABLE, FUNCTION
};

struct Declaration {
    DeclarationKind kind;
    union content {
        StructDeclaration* strucct;
        DataDeclaration* data;
        VariableDeclaration* variable;
        FunctionDeclaration* function;
    };
};

struct StructDeclaration {
    TypeDef *typeDef;
    std::vector<Declaration*> declarations;

    StructDeclaration() : typeDef{nullptr}, declarations{}
    {

    }
};

struct TypeDef {
    std::string typeName;
    std::vector<std::string> typeArgs;

    TypeDef(std::string typeName) : typeName{typeName}, typeArgs{}
    {

    }
};

struct DataDeclaration {

};

struct VariableDeclaration {

};

struct FunctionDeclaration {

};

}


#endif