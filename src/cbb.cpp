#include "antlr4-runtime.h"

#include "parser.h"
#include "CbbLexer.h"
#include "CbbParser.h"
#include "CbbBaseVisitor.h"
#include "semantic_validator.h"

#include <iostream>
#include <fstream>

using namespace antlr4;

using namespace cbb;

int main( int argc, char** argv)
{
    if(argc < 2)
        return 1;
    std::ifstream in;
    in.open(argv[1]);
    ANTLRInputStream input(in);
    CbbLexer lexer(&input);
    CommonTokenStream tokens(&lexer);
    CbbParser parser(&tokens);
    ParserListener pl;
    parser.addParseListener(&pl);
    auto tree = parser.file();
    SemanticValidator sv;
    auto m = sv.visitFile(tree).as<Module*>();
    return 0;
}