#ifndef CBB_PARSER_H
#define CBB_PARSER_H


#include "CbbBaseListener.h"

#include <fstream>
#include <vector>
#include <cstdint>
#include <cstddef>

namespace cbb {
    class ParserListener : public CbbBaseListener{
        private:
            
        public:
            ParserListener() = default;
            ~ParserListener() {};
            virtual void exitFile(CbbParser::FileContext *ctx);
    };
}

#endif