#ifndef CBB_SEMANTIC_VALIDATOR_H
#define CBB_SEMANTIC_VALIDATOR_H

#include "CbbBaseVisitor.h"

#include "antlr4-runtime.h"

#include <string>
#include <vector>
#include <stack>
#include <map>

namespace cbb {

enum class FunctionModifier {
    PURE
};

enum class DeclarationVisibility {
    EXPORT, HIDDEN
};

enum class DeclKind {
    FUNCTION, VARIABLE, VALUE, STRUCT
};

enum class TypeKind {
    MODIFIED, NORMAL
};

enum class ModifiedTypeKind {
    POINTER, SLICE, ARRAY, MULTIPOINTER
};

enum class ExprKind {
    LITERAL, ARRAY, STRUCT, NAMED, UNARY, BINARY, TERTIARY
};

struct Type;

struct Expr;

struct TypeName {
    std::string basicName;
    std::vector<std::string> qualifiers;
};

struct QualifiedName {
    std::string basicName;
    std::vector<std::string> qualifiers;
};

struct AssignmentStmt {
    QualifiedName *assignee;
    Expr *assignedValue;
};

struct StructExpr {
    TypeName *name;
    std::vector<AssignmentStmt*> assignments;
};

struct ArrayExpr {
    std::vector<Expr*> expressions;
};

enum class LiteralExprKind {
    BOOL, INT, FLOAT, CHAR, STRING
};

enum class IntLiteralKind {
    BIN, HEX, DEC
};

struct IntLiteral {
    IntLiteralKind kind;
    std::string value;
    size_t width;
};

struct FloatLiteral {
    std::string value;
    size_t width;
};

struct LiteralExpr {
    LiteralExprKind kind;
    union {
        bool boolLiteral;
        std::string *stringLiteral;
        IntLiteral *intLiteral;
        FloatLiteral *floatLiteral;
    } expr;
};

struct Expr {
    Type *type;
    ExprKind kind;
    union {
        StructExpr *structExpr;
        ArrayExpr *arrayExpr;
        QualifiedName *namedExpression;
        LiteralExpr *literalExpr;
    } expr;
};


struct ModifiedType {
    ModifiedTypeKind kind;
    Type *child;
    size_t length;
};

struct BaseType {
    TypeName name;
    std::vector<Type*> typeArgs;
};

struct Type {
    TypeKind kind;
    union {
        ModifiedType* modified;
        BaseType *baseType;
    } type;
};

struct Symbol {
    std::string name;
    std::vector<std::string> type;
};

struct Scope {
    Scope *parent;
    std::vector<Symbol> symbols;

    Type* resolveName();
};

struct Parameter {
    std::string name;
    Type *type;
};

struct FuncDecl {
    Scope symbols;
    FunctionModifier modifier;
    std::string name;
    std::vector<Parameter*> params;
};

struct Declaration {
    DeclKind kind;
    DeclarationVisibility visibility;
    union {
        FuncDecl *func;
    } decl;
};

struct Module {
    std::string name;
    Scope scope;
    std::vector<Declaration*> decls;
    std::vector<Symbol> exports;
};

class SemanticValidator : public CbbBaseVisitor {
    public:
        antlrcpp::Any visitFile(CbbParser::FileContext *ctx);

        antlrcpp::Any visitFuncDecl(CbbParser::FuncDeclContext *context);

        antlrcpp::Any visitDecl(CbbParser::DeclContext *ctx);

        antlrcpp::Any visitParamList(CbbParser::ParamListContext *context);

        antlrcpp::Any visitParam(CbbParser::ParamContext *context);

        antlrcpp::Any visitType(CbbParser::TypeContext *context);

        antlrcpp::Any visitTypeName(CbbParser::TypeNameContext *ctx);

        antlrcpp::Any visitQualifiedType(CbbParser::QualifiedTypeContext *ctx);

        antlrcpp::Any visitTypeArgs(CbbParser::TypeArgsContext *ctx);

        antlrcpp::Any visitExprList(CbbParser::ExprListContext *ctx);

        antlrcpp::Any visitExpr(CbbParser::ExprContext *ctx);

        antlrcpp::Any visitStructExpr(CbbParser::StructExprContext *ctx);

        antlrcpp::Any visitArrayExpr(CbbParser::ArrayExprContext *ctx);

        antlrcpp::Any visitQualifiedName(CbbParser::QualifiedNameContext *ctx);

        antlrcpp::Any visitTypeModifier(CbbParser::TypeModifierContext *ctx);

        antlrcpp::Any visitAssList(CbbParser::AssListContext *ctx);

        antlrcpp::Any visitAssStmt(CbbParser::AssStmtContext *ctx);
    private:
        Module m;
        std::stack<Scope*> scopes;
        DeclarationVisibility defaultVisibility;
};
}

#endif